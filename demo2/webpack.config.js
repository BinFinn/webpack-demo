// webpack.config.js
var path = require('path');
module.exports = {
	entry: {
		main1: './main1.js',
		main2: './main2.js'
	},
	output: {
        path: path.resolve(__dirname, './'),
        publicPath: '/',
        filename: '[name].bundle.js'
	}
}