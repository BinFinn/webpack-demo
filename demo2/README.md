# Demo2


#### 安装教程

1. 目录结构

   ```
   demo2
   ├─main1.bundle.js // 产出文件，可以不用新建
   ├─main2.bundle.js // 产出文件，可以不用新建
   ├─index.html // 手动创建
   ├─main1.js // 手动创建
   ├─main2.js // 手动创建
   ├─package.json // 通过命令创建
   └─webpack.config.js // 手动创建
   ```

2. 创建项目目录

   ```bash
   mkdir demo2
   cd demo2
   ```

3. 初始化一个默认的package.json

   ```bash
   npm init -y
   你也可以运行[npm init]来自己配置具体信息
   ```

   `注：如果安装了全局webpack，则4、5步骤不需要执行也可。`

4. 安装webpack

   ```bash
   //全局安装
   npm install webpack --g
   //安装到项目依赖中（推荐）
   npm install webpack --save-dev
   ```

5. 如果你使用 webpack 4+ 版本，你还需要安装 CLI

   ```bash
   npm install --save-dev webpack-cli
   ```

6. 新建一个main1.js、main2.js作为入口文件，并输入

   ```javascript
   // demo1.js
   document.write('Hello World!--demo1.js <br>');
   // demo2.js
   document.write('Hello World!--demo2.js');
   ```

7. 新建一个webpack.config.js文件，用来配置webpack。内容参考如下

   ```javascript
   // webpack.config.js
   var path = require('path');
   module.exports = {
   	entry: {
   		main1: './main1.js',
   		main2: './main2.js'
   	},
   	output: {
           path: path.resolve(__dirname, './'),
           publicPath: '/',
           filename: '[name].bundle.js'
   	}
   }
   ```

8. 新建index.html文件

   ```html
   <html>
     <body>
       <script src="main1.bundle.js"></script>
       <script src="main2.bundle.js"></script>
     </body>
   </html>
   ```

9. 修改package.json文件内容

   ```json
"scripts": {
    "dev": "webpack-dev-server --open",// 以服务的方式并在浏览器中启动
    "test": "echo \"Error: no test specified\" && exit 1"
}
   ```

11. 运行

    ```bash
    webpack //直接使用webpack命令打包，成功后根目录会生成一个bundle.js文件
    npm run dev // 通过调用scripts配置的命令执行
    注：这里不需要运行npm install
    ```

12. 打开index.html

    ```bash
    页面输出:
    Hello World!--demo1.js 
    Hello World!--demo2.js
    ```
