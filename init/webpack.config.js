// webpack.config.js
var path = require('path');
//var webpack = require('webpack'); // 如果引入webpack就必须安装webpack，否则可以直接使用全局webpack命令
module.exports = {
	// 入口配置
    entry: {
        main: './src/main.js'
    },
	// 输出配置
	output: {
        path: path.resolve(__dirname, './dist'),
        publicPath: '',
        filename: 'bundle.js'
    }
}