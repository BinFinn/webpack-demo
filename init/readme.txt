目录结构：
├─src // 自定义入口文件存放位置
└─package.json // npm配置文件
└─webpack.config.js // webpack配置文件

安装并运行：
1、安装(如果webpack.config.js未引入webpack则不需要安装)
    npm install
    cnpm install
2、运行
    webpack
3、此时根目录会生成dist文件夹里面包含输出配置的文件