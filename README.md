# Webpack学习

#### 介绍
Webpack学习

#### 软件架构
Webpack


#### 安装教程

在开始学习Webpack之前，请先确保安装了Node.js，建议安装最新版的Node.js。然后就可以使用npm安装Webpack了。你可以将Webpack安装到全局，不过我们通常会把它安装到项目依赖中。

1. 创建项目目录

   ```bash
   mkdir init
   cd init
   ```

2. 初始化一个默认的package.json

   ```bash
   npm init -y
   你也可以运行[npm init]来自己配置具体信息
   ```

3. 安装webpack

   ```bash
   //全局安装
   npm install webpack --g
   //安装到项目依赖中（推荐）
   npm install webpack --save-dev
   ```

4. 如果你使用 webpack 4+ 版本，你还需要安装 CLI。

   ```bash
   npm install --save-dev webpack-cli
   ```

5. 安装webpack-dev-server

   ```bash
   //全局安装
   npm install webpack-dev-server --g
   //安装到项目依赖中（推荐）
   npm install webpack-dev-server --save-dev
   ```

6. 新建一个webpack.config.js文件，用来配置webpack。内容参考如下

   ```javascript
   // webpack.config.js
   // var webpack = require('webpack');
   var path = require('path');
   module.exports = {
       entry: {
           app: './src/main.js'
       },
       output: {
           path: path.resolve(__dirname, './'),
           publicPath: '',
           filename: 'bundle.js'
       }
   }
   ```

7. 修改package.json文件内容

   ```javascript
   "scripts": {
       "dev": "webpack-dev-server --open"
   }
   ```

8. 运行

   ```bash
   webpack //直接使用webpack命令打包
   npm run dev // 通过调用scripts配置的命令执行
   ```

9. Some command-line options you should know.

   ```bash
   webpack // building for development
   webpack -p // building for production (minification)
   webpack --watch // for continuous incremental building
   webpack -d // including source maps
   webpack --colors // making building output pretty
   ```
   
   上面仅是webpack环境配置的一个流程。不作为真实项目的实际配置。
   
   其它命令使用：
   
   ```
   清理 /dist 文件夹
   npm install clean-webpack-plugin --save-dev
   ```

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 feat_branch 分支
3. 提交代码
4. 新建 Pull Request

#### 参考

1. https://www.jianshu.com/p/69425db979f1
2. https://github.com/ruanyf/webpack-demos
3. https://blog.csdn.net/userkang/article/details/83504048


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)