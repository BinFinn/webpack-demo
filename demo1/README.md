# Demo1


#### 安装教程

1. 目录结构

   ```
   demo1
   ├─bundle.js // 产出文件，可以不用新建
   ├─index.html
   ├─main.js
   ├─package.json
   └─webpack.config.js
   ```

2. 创建项目目录

   ```bash
   mkdir demo1
   cd demo1
   ```

3. 初始化一个默认的package.json

   ```bash
   npm init -y
   你也可以运行[npm init]来自己配置具体信息
   ```

4. 安装webpack

   ```bash
   //全局安装
   npm install webpack --g
   //安装到项目依赖中（推荐）
   npm install webpack --save-dev
   ```

5. 如果你使用 webpack 4+ 版本，你还需要安装 CLI

   ```bash
   npm install --save-dev webpack-cli
   ```

6. 安装webpack-dev-server

   ```bash
   //全局安装
   npm install webpack-dev-server --g
   //安装到项目依赖中（推荐）
   npm install webpack-dev-server --save-dev
   ```

7. 新建一个main.js作为入口文件，并输入

   ```javascript
   document.write('<h1>Hello World</h1>');
   ```

8. 新建一个webpack.config.js文件，用来配置webpack。内容参考如下

   ```javascript
   // webpack.config.js
   // var webpack = require('webpack');
   var path = require('path');
   module.exports = {
       entry: {
           app: './main.js'// 对应第6条的main.js，这里我放在了根目录
       },
       output: {
           path: path.resolve(__dirname, './'),
           publicPath: '',
           filename: 'bundle.js'// 输出文件，这里我将输出的文件放在了根目录
       }
   }
   ```

9. 修改package.json文件内容

   ```javascript
   "scripts": {
       "dev": "webpack-dev-server --open"// 以服务的方式并在浏览器中启动
   }
   ```

10. 新建index.html文件

   ```html
   <html>
     <body>
       <!-- 引入产出文件 -->   
       <script type="text/javascript" src="./bundle.js"></script>
     </body>
   </html>
   ```

11. 运行

    ```bash
    webpack //直接使用webpack命令打包，成功后根目录会生成一个bundle.js文件
    npm run dev // 通过调用scripts配置的命令执行
    注：这里不需要运行npm install
    ```

12. 打开index.html

    ```bash
    页面输出 Hello World
    ```


#### 知识总结

```bash
scripts配置：（注意正式文件中不可以添加注释信息）
"scripts": {
  "dev": "webpack-dev-server --open", // 以服务的方式并在浏览器中启动
  "build": "webpack -p", // 打包
  "dev": "webpack-dev-server --devtool eval --progress --colors",
  "deploy": "NODE_ENV=production webpack -p",
  "build": "webpack --mode production",
  "test": "echo \"Error: no test specified\" && exit 1"
}
注：
webpack：开发环境构建
webpack -p：生产环境构建(压缩混淆脚本)
webpack --watch：监听变动并自动打包
webpack -d：生成 map 映射文件
webpack --colors：构建过程带颜色输出
```

